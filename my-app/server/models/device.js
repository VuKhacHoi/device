var mongoose = require('mongoose');

module.exports= mongoose.model('divice',{
    name:{type: String, required: true},
    image :
         {
            title:{type: String, required: false},
            url:{type: String, required: false}
         }
    ,
    category:[String],
    location:{type: String, required: false},
    quantity:{type: Number, required: false},
    detail:{type: String, required: false},

   });