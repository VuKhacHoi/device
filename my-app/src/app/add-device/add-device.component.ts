import { Component, OnInit ,Input } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DeviceService} from '../../services/deviceService';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.css']
})
export class AddDeviceComponent implements OnInit {

  constructor( private deviceService:DeviceService) { }

  ngOnInit() {
  }

    @Input() bsModal :BsModalRef ;
    name;
    location;
    category;
    quantity;
    detail;

    AddDevice = new FormGroup({
        name: new FormControl('', [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(30),

        ]),
        location: new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(30)

        ]),
        category: new FormControl('', [
            Validators.required,




        ]),
        quantity: new FormControl('', [
            Validators.required,
            Validators.min(1),
            Validators.max(100),


        ]),
        detail: new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(30),


        ])

    });


   get getName(){
       return  this.AddDevice.get('name');
   }


    get getLocation(){
        return  this.AddDevice.get('location');
    }

    get getCategory(){
        return  this.AddDevice.get('category');
    }

    get getDetail(){
        return  this.AddDevice.get('detail');
    }

    get getQuantity(){
        return  this.AddDevice.get('quantity');
    }


    categories: string[] = [
"IOS","Phone","Cell Phone","Computer","Telecommunications","Monitors","Keyboards","Printers","Servers","Drives","Network HUBs"
    ]


  addDevice(){
      this.deviceService.createDevice({

          "name": this.name,
          "location": this.location,
          "category": [
          this.category
      ],
          "quantity": this.quantity,
          "detail": this.detail

      })

this.bsModal.hide();

  }



    Cancel() {

this.bsModal.hide();
    }



}
