import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app/app.component';
import { HomeComponent } from './home/home.component';
import { ModalModule } from 'ngx-bootstrap';
import { AdminComponent } from './admin/admin.component';
import { CardComponent } from './card/card.component';
import { EditDeviceComponent } from './edit-device/edit-device.component';
import { UserOutletComponent } from './user-outlet/user-outlet.component';
import { NavComponent } from './nav/nav.component';
import { AdminCardComponent } from './admin-card/admin-card.component';
import { CarouselModule } from 'ngx-bootstrap';
import { DeviceComponent } from './device/device.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { UserService } from '../services/userService';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { PendingService } from '../services/pendingService';
import { DeviceService } from '../services/deviceService';
import { CompareValidatorDirective } from './shared/compare-validator.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NgxPageScrollModule} from 'ngx-page-scroll';
import {SharedService} from '../services/shareService';
import {AdminAuthGuardService1} from '../services/AdminAuthGuardService1';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';

import { UserProfileComponent } from './user-profile/user-profile.component';
import { DeviceAdminComponent } from './device-admin/device-admin.component';
import { ImageAdminComponent } from './image-admin/image-admin.component';
import { PendingAdminComponent } from './pending-admin/pending-admin.component';
import { ItemAdminComponent } from './item-admin/item-admin.component';
import { AddDeviceComponent } from './add-device/add-device.component';
import { TypeaheadModule } from 'ngx-bootstrap';

const routes: Routes = [
  {
    path : 'admin',
    component: AdminComponent,
      canActivate: [AdminAuthGuardService1],
    children: [
        { path: '', redirectTo: 'dashboard', pathMatch: 'full', canActivate: [AdminAuthGuardService1]},
        { path: 'dashboard', component: DashboardComponent, canActivate: [AdminAuthGuardService1] },
        { path: 'image', component: ImageAdminComponent , canActivate: [AdminAuthGuardService1] },
        { path: 'device', component: DeviceAdminComponent  , canActivate: [AdminAuthGuardService1] },
        { path: 'pending', component: PendingAdminComponent , canActivate: [AdminAuthGuardService1] },
        { path: 'user', component: UserProfileComponent , canActivate: [AdminAuthGuardService1]},
        { path: '**', redirectTo: 'dashboard' , canActivate: [AdminAuthGuardService1] },
    ]
  },
  {

    path : '',
  component: UserOutletComponent,
  children: [

    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: '**', redirectTo: 'home' },

  ]
  }


];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdminComponent,
    CardComponent,
    EditDeviceComponent,
    UserOutletComponent,
    NavComponent,
    AdminCardComponent,
    DeviceComponent,
    AboutComponent,
    ContactComponent,
    FooterComponent,
    CompareValidatorDirective,
    DashboardComponent,
    UserProfileComponent,
    DeviceAdminComponent,
    ImageAdminComponent,
    PendingAdminComponent,
    ItemAdminComponent,
    AddDeviceComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpModule,
    ReactiveFormsModule,
      BrowserAnimationsModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forRoot(routes),
    CarouselModule.forRoot(),
      BsDatepickerModule.forRoot(),
      NgxPageScrollModule,
      TypeaheadModule.forRoot()
  ],
  providers: [
   UserService,
   PendingService,
   DeviceService,
      SharedService,
      AdminAuthGuardService1
  ],
  bootstrap: [AppComponent]
})



export class AppModule { }
