import {Component, OnInit, Input, TemplateRef} from '@angular/core';
import { device } from '../../model/device';
import {animate, state, style, transition, trigger, keyframes} from '@angular/animations';
import {SharedService} from '../../services/shareService';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
    animations: [
        trigger('translate', [

            transition('a=>b', animate('3000ms ease-in', keyframes([
                style({ opacity: 0, transform: 'translateY(-100px)', offset: 0 }),
                style({ opacity: 1, transform: 'translateY(0px)', offset: 0.5 }),

            ])))
            ,

            transition(':leave', animate('2000ms ease-out', keyframes([
                style({ opacity: 1, transform: 'translateY(0px)', offset: 0 }),
                style({ opacity: 0, transform: 'translateY(-60px)', offset: 0.5 }),

            ])))

        ]),
        trigger('popOverState', [
            state('show', style({
                filter: 'brightness(130%)',
                opacity: 1,
                boxShadow: '1px 2px 4px rgba(0, 0, 0, .5)'
            })  ),
            state('hide', style({
                opacity: 1
            })  ),
            transition('show=>hide', animate('300ms ease-out')),
            transition('hide=>show', animate('300ms ease-in'))


        ])

    ]
  })
  export class CardComponent implements OnInit {

  @Input() device: device;
  constructor(private  sharedService: SharedService,
              private  modalService: BsModalService) {
      this.sharedService.changeNavEmitted.subscribe(i => this.initial = i.index === 1 ? true: false );

      this.bsRangeValue = [this.bsValue, this.maxDate];
  }
   show = false;
initial = false;
    modalRef: BsModalRef;


    get stateA() {
        return this.initial ? 'b' : 'a';
    }
  get stateName() {
    return this.show ? 'show' : 'hide';
  }
    toggle() {
        this.show = !this.show;
    }

    book() {

    }

  ngOnInit() {
  }
    bsValue = new Date();
    bsRangeValue: Date[];
    maxDate = new Date();




}
