import { Component, TemplateRef ,OnInit} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'app-device-admin',
  templateUrl: './device-admin.component.html',
  styleUrls: ['./device-admin.component.css']
})
export class DeviceAdminComponent implements OnInit {

    modalRef: BsModalRef;
    constructor(private modalService: BsModalService) {}

    openModalWithClass(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-lg' })
        );
    }

  ngOnInit() {
  }





}
