import { Component, OnInit } from '@angular/core';
import { device } from '../../model/device';
@Component({
  selector: 'device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {

  constructor() { }
  device:device;
  ngOnInit() {
    this.device=new device("1","Microphone","asset/board",10);
  }

}
