import {Component, Input, OnInit} from '@angular/core';
import {animate, keyframes, style, transition, trigger} from '@angular/animations';

@Component({
    selector: 'item-admin',
    templateUrl: './item-admin.component.html',
    styleUrls: ['./item-admin.component.css'],
    animations: [
        trigger('hoverState', [
            transition('hide=>show', animate('1500ms ease-in', keyframes([
                style({ opacity: 0 , transform: 'scale(1)', offset: 0 }),
                style({ opacity: 1, transform: 'scale(1.5)', offset: 0.33 }),
                style({ opacity: 0.8, transform: 'scale(1)', offset: 0.66 }),
            ])))
        ])
    ]
})
export class ItemAdminComponent implements OnInit {
    @Input()route: string ;
    @Input()value: string ;
    @Input()icon: string ;
    constructor() { }
    show = false;
    get getState() {
        return this.show ? 'show' : 'hide';
    }


    ngOnInit() {
    }

}
