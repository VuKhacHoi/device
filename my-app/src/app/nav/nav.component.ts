import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { UserService } from '../../services/userService';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { catchError } from 'rxjs/operators';
import { DeviceService } from '../../services/deviceService';
import {user} from '../../model/user';
import {SharedService} from '../../services/shareService';
import {animate, keyframes, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
    animations: [
        trigger('menuState', [
            transition(':enter', animate('1000ms ease-in', keyframes([
                style({ opacity: 0, transform: 'translateY(-50px)', offset: 0 }),
                style({ opacity: 1, transform: 'translateY(0px)', offset: 0.5 }),

            ]))),
            transition(':leave', animate('1000ms ease-in', keyframes([
                style({ opacity: 1, transform: 'translateY(0px)', offset: 0 }),
                style({ opacity: 0, transform: 'translateY(-50px)', offset: 0.5 }),
  
            ])))
        ])
            ]
})
export class NavComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl('', [
     Validators.required,
     Validators.minLength(3),
     Validators.maxLength(30),

    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(30)

     ])
  });


  RegisterForm = new FormGroup({
    usernameR: new FormControl('', [
     Validators.required,
     Validators.minLength(3),
     Validators.maxLength(30),

    ]),
    passwordR: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(30)

     ]),
      confirmPasswordR: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(30),



     ]),
     emailR: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(30),
      Validators.email

     ])
  });







  get getUsername() {
    return  this.loginForm.get('username');
   }
   get getPassword() {
    return  this.loginForm.get('password');
   }
   get getPasswordR() {
    return  this.RegisterForm.get('passwordR');
   }
   get getUsernameR() {
    return  this.RegisterForm.get('usernameR');
   }
   get getCategory() {

    return  this.RegisterForm.get('confirmPasswordR');
   }
   get getEmailR() {
    return  this.RegisterForm.get('emailR');
   }



  constructor(private modalService: BsModalService,
              private userService: UserService,
              private deviceService: DeviceService,
              private  sharedService: SharedService) {



  }


  changeUser() {
      const  token = localStorage.getItem('token');


      this.userService.getUserById(token).pipe(
          catchError((err, caught) => {
              return err;
          })
      ).subscribe(u => {
          const  user = JSON.parse(u._body);
          this.usernameI = user.username;

      });
  }

  ngOnInit() {
      this.changeUser();


  }

  active = 0;
  navs = [
    {
      title : 'Home',
      url: '#home'
    },
    {
      title: 'Devices',
      url: '#devices'
    },
    {
      title: 'Contact',
      url: '#contact'
    },
    {
      title: 'About',
      url: '#about'
    },
    {
      title: 'Register',
      url: '#home'
    },

    {
      title: 'Login',
      url: '#home'
    }



  ];
  hover = false;
  modalRef: BsModalRef;
  usernameL;
  passwordL;
  usernameI = 'Vu Khac Hoi';
 usernameR;
 passwordR;
 confirmPasswordR;
 emailR;
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  isLogin = false;



  showLogin(index) {


    if (( index === 5 || index === 4) && (this.isLogin || localStorage.getItem('token') != null)) {
    return false;
    }
    return true;
  }


  activeClick(_active, Login, Register) {
  this.active = _active;

  this.sharedService.emitChangeNav({'index': _active});

  if (_active === 5) {
    this.openModal(Login);

  } else if (_active === 4) {
    this.openModal(Register);
  }

  }
  // "email":"todilahoi01017@gmail.com",
  //     "password":"123456789"

  login() {
    this.userService.login({
      'email': this.usernameL,
      'password': this.passwordL
      }).pipe(
        catchError((err, caught) => {
        console.log(err);
        return err;
      })).subscribe(u => {

          this.modalRef.hide();
          this.loginForm.reset();
         const body = JSON.parse(u._body);
         localStorage.setItem('token', body.token);
         this.active = 0;
         this.isLogin = true;
         this.changeUser();
      }
      );
  }


  cancel() {
    this.loginForm.reset();
    this.modalRef.hide();
  }


  register() {
      // if (!this.RegisterForm.invalid) {
          this.userService.registerUser({
              'email': this.emailR,
              'password': this.passwordR,
              'username': this.usernameR
          }).subscribe(u => {
              this.modalRef.hide();
              this.RegisterForm.reset();
          });
      // }
  }


  logout() {
     localStorage.removeItem('token');
     this.usernameI = 'TMA Solution';
this.isLogin=false;
  }


}
