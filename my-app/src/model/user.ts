import {image} from './image';
export class user{
    id: string;
    username:string;
    password:string;
    email:string;
    department:string;
    hometown:string;
    identity_number:string;
    image:image;
    role:string;
    devices : [{
        name:string;
        image:image;
        borrowed_day:string;
        enpired_day:string;
    }]


    constructor(
        username:string,
        password:string,
        email:string,
        department:string,
        hometown:string,
        identity_number:string){
            this.username=username;
            this.password=password;
            this.email=email;
            this.department=department;
            this.hometown=hometown;
            this.identity_number=identity_number;
            this.role="user";

    }
}