import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { UserService } from './userService';


import {catchError, map, mergeMap} from 'rxjs/operators';
import {Observable, of } from 'rxjs';


@Injectable()
export class AdminAuthGuardService1 implements CanActivate {

    constructor(private userService: UserService, private router: Router) { }

    canActivate(): Observable<boolean> |boolean|any {
        const token = localStorage.getItem('token');

        return this.userService.getUserById(token).pipe(
            map(u => u),
            mergeMap(n => {
                const  user = JSON.parse(n._body);
                 user.role === 'admin'?null: this.router.navigate(['/**']);
                return  of(user.role === 'admin');
            }),
            catchError(() => {
                // this.router.navigate(['/**']);
                return of(false);
            })
        );


    }
}
