import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { user } from '../model/user';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from '../../node_modules/rxjs';
import {tokenize} from 'ngx-bootstrap/typeahead';



@Injectable({
  providedIn: 'root'
})
export class DeviceService  {

  constructor(private http: Http,
  ) { }


  private log(message: string) {
   console.log(message);
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
   
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
   
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  // get deivce using localhost:3000/api/user
  getDevice(){
    var token=localStorage.getItem("token");
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append("x-access-token",token);
    let options = new RequestOptions({ headers: headers }); 
    return  this.http.get('/api/api/device',options).pipe(
    map(d=>d.json()),
    tap(WSAEUSERS=>console.log("get  successful data")),
    catchError(this.handleError('getHeroes', []))
  ).subscribe(devices=>console.log(devices));
  }


 


 


createDevice(device)
{
   const token=localStorage.getItem("token");
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });
    headers.append("x-access-token",token);
  this.http.post('/api/api/device',device,options).pipe(r=>r).subscribe(d=>console.log(d));
}



updateDevice(device,token)
{
  let headers = new Headers({ 'Content-Type': 'application/json' });
  headers.append("x-access-token",token);
  let options = new RequestOptions({ headers: headers }); 
  this.http.put('/api/api/device/'+device.id,device,options).pipe(r=>r).subscribe(d=>console.log(d));
}



deleteDevice(id,token)
{
  let headers = new Headers({ 'Content-Type': 'application/json' });
headers.append("x-access-token",token);
  let options = new RequestOptions({ headers: headers }); 
  this.http.delete('/api/api/device/'+id,options).pipe(r=>r).subscribe(d=>console.log(d));
}
}
