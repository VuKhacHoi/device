import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { user } from '../model/user';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from '../../node_modules/rxjs';



@Injectable({
  providedIn: 'root'
})
export class PendingService  {

  constructor(private http: Http,
  ) { }


  private log(message: string) {
   console.log(message);
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      //  Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  // get deivce using localhost:3000/api/user
  getPendingDevices(token) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('x-access-token', token);
    const options = new RequestOptions({ headers: headers });
    return  this.http.get('/api/api/spending', options).pipe(
    map(d => d.json()),
    tap(WSAEUSERS => console.log('get  successful data')),
    catchError(this.handleError('getHeroes', []))
  ).subscribe(devices => console.log(devices));
  }








createPending(pending): Observable<any> {
    const token = localStorage.getItem('token');
    const headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('x-access-token', token);
    const options = new RequestOptions({ headers: headers });
   return this.http.post('/api/api/spending', pending, options).
           pipe( map(r => r.json()),
                     catchError(this.handleError('err', []))
       );
}



updatePending(pending, token) {
  const headers = new Headers({ 'Content-Type': 'application/json' });
  headers.append('x-access-token', token);
  const options = new RequestOptions({ headers: headers });
  this.http.put('/api/api/spending/' + pending.id, pending, options).pipe(r => r).subscribe(d => console.log(d));
}



deleteDevice(id, token) {
  const headers = new Headers({ 'Content-Type': 'application/json' });
headers.append('x-access-token', token);
  const options = new RequestOptions({ headers: headers });
  this.http.delete('/api/api/spending/' + id, options).pipe(r => r).subscribe(d => console.log(d));
}
}
