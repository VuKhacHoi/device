
import { Injectable } from '@angular/core';
import {Subject, Observable} from 'rxjs';

@Injectable()
export class SharedService {


    // value change token
    private emitChangeTokenSource = new Subject<any>();

    changeNavEmitted = this.emitChangeTokenSource.asObservable();
    emitChangeNav(change: any) {
        this.emitChangeTokenSource.next(change);
    }


}
