import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { user } from '../model/user';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from '../../node_modules/rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService  {

  constructor(private http: Http,
  ) { }
  

  private log(message: string) {
   console.log(message);
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  // get deivce using localhost:3000/api/user
  getUser() {

  return  this.http.get('/api/api/user').pipe(
    map(d => d.json()),
    tap(WSAEUSERS => console.log('get  successful data')),
    catchError(this.handleError('getHeroes', []))
  ).subscribe(devices => console.log(devices));
  }






  // user login

login(user): Observable<any> {
  const headers = new Headers({ 'Content-Type': 'application/json' });

  const options = new RequestOptions({ headers: headers });
 return this.http.post('/api/login', user, options);
}


registerUser (user): Observable<any> {
  const headers = new Headers({ 'Content-Type': 'application/json' });
  const options = new RequestOptions({ headers: headers });
return  this.http.post('/api/user', user, options).pipe(
      map(d => d.json()),
      catchError(this.handleError('err', []))
  );
}



updateUser(user, token) {
  const headers = new Headers({ 'Content-Type': 'application/json' });
  headers.append('x-access-token', token);
  const options = new RequestOptions({ headers: headers });
  this.http.put('/api/api/user/' + user.id, user, options).pipe(r => r).subscribe(d => console.log(d));
}



deleteUser(id, token) {
  const headers = new Headers({ 'Content-Type': 'application/json' });
headers.append('x-access-token', token);
  const options = new RequestOptions({ headers: headers });
  this.http.delete('/api/api/user/' + id, options).pipe(r => r).subscribe(d => console.log(d));
}




getUserById(token): Observable<any> {
  const headers = new Headers({ 'Content-Type': 'application/json' });
  headers.append('x-access-token', token);
  const options = new RequestOptions({ headers: headers });
  return this.http.get('/api/api/userid/', options);
}
}
